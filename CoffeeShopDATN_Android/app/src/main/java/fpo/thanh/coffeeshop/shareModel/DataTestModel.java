package fpo.thanh.coffeeshop.shareModel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DataTestModel {
    @SerializedName("_id")
    @Expose
    private String id;
    @SerializedName("username")
    @Expose
    private String username;
    @SerializedName("password")
    @Expose
    private String password;
    @SerializedName("fullname")
    @Expose
    private String fullname;
    @SerializedName("gender")
    @Expose
    private Integer gender;
    @SerializedName("address")
    @Expose
    private String address;
    @SerializedName("phone")
    @Expose
    private String phone;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("avatar")
    @Expose
    private String avatar;
    @SerializedName("soTheDoanVien")
    @Expose
    private String soTheDoanVien;
    @SerializedName("trinhDoChuyenMon")
    @Expose
    private String trinhDoChuyenMon;
    @SerializedName("ngoaiNgu")
    @Expose
    private String ngoaiNgu;
    @SerializedName("danToc")
    @Expose
    private String danToc;
    @SerializedName("tonGiao")
    @Expose
    private String tonGiao;
    @SerializedName("lyLuanChinhTri")
    @Expose
    private String lyLuanChinhTri;
    @SerializedName("noiKetNapDoan")
    @Expose
    private String noiKetNapDoan;
    @SerializedName("noiThucTap")
    @Expose
    private String noiThucTap;
    @SerializedName("noiLamViec")
    @Expose
    private String noiLamViec;
    @SerializedName("classId")
    @Expose
    private String classId;
    @SerializedName("departmentId")
    @Expose
    private String departmentId;
    @SerializedName("majorId")
    @Expose
    private String majorId;
    @SerializedName("birthday")
    @Expose
    private String birthday;
    @SerializedName("ngayVaoDangDuBi")
    @Expose
    private String ngayVaoDangDuBi;
    @SerializedName("ngayVaoDangChinhThuc")
    @Expose
    private String ngayVaoDangChinhThuc;
    @SerializedName("ngayVaoDoan")
    @Expose
    private String ngayVaoDoan;
    @SerializedName("departmentName")
    @Expose
    private String departmentName;
    @SerializedName("majorName")
    @Expose
    private String majorName;
    @SerializedName("className")
    @Expose
    private String className;
    @SerializedName("levelMax")
    @Expose
    private Integer levelMax;
    @SerializedName("levelMin")
    @Expose
    private Integer levelMin;
    @SerializedName("iat")
    @Expose
    private Integer iat;
    @SerializedName("exp")
    @Expose
    private Integer exp;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public Integer getGender() {
        return gender;
    }

    public void setGender(Integer gender) {
        this.gender = gender;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getSoTheDoanVien() {
        return soTheDoanVien;
    }

    public void setSoTheDoanVien(String soTheDoanVien) {
        this.soTheDoanVien = soTheDoanVien;
    }

    public String getTrinhDoChuyenMon() {
        return trinhDoChuyenMon;
    }

    public void setTrinhDoChuyenMon(String trinhDoChuyenMon) {
        this.trinhDoChuyenMon = trinhDoChuyenMon;
    }

    public String getNgoaiNgu() {
        return ngoaiNgu;
    }

    public void setNgoaiNgu(String ngoaiNgu) {
        this.ngoaiNgu = ngoaiNgu;
    }

    public String getDanToc() {
        return danToc;
    }

    public void setDanToc(String danToc) {
        this.danToc = danToc;
    }

    public String getTonGiao() {
        return tonGiao;
    }

    public void setTonGiao(String tonGiao) {
        this.tonGiao = tonGiao;
    }

    public String getLyLuanChinhTri() {
        return lyLuanChinhTri;
    }

    public void setLyLuanChinhTri(String lyLuanChinhTri) {
        this.lyLuanChinhTri = lyLuanChinhTri;
    }

    public String getNoiKetNapDoan() {
        return noiKetNapDoan;
    }

    public void setNoiKetNapDoan(String noiKetNapDoan) {
        this.noiKetNapDoan = noiKetNapDoan;
    }

    public String getNoiThucTap() {
        return noiThucTap;
    }

    public void setNoiThucTap(String noiThucTap) {
        this.noiThucTap = noiThucTap;
    }

    public String getNoiLamViec() {
        return noiLamViec;
    }

    public void setNoiLamViec(String noiLamViec) {
        this.noiLamViec = noiLamViec;
    }

    public String getClassId() {
        return classId;
    }

    public void setClassId(String classId) {
        this.classId = classId;
    }

    public String getDepartmentId() {
        return departmentId;
    }

    public void setDepartmentId(String departmentId) {
        this.departmentId = departmentId;
    }

    public String getMajorId() {
        return majorId;
    }

    public void setMajorId(String majorId) {
        this.majorId = majorId;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public String getNgayVaoDangDuBi() {
        return ngayVaoDangDuBi;
    }

    public void setNgayVaoDangDuBi(String ngayVaoDangDuBi) {
        this.ngayVaoDangDuBi = ngayVaoDangDuBi;
    }

    public String getNgayVaoDangChinhThuc() {
        return ngayVaoDangChinhThuc;
    }

    public void setNgayVaoDangChinhThuc(String ngayVaoDangChinhThuc) {
        this.ngayVaoDangChinhThuc = ngayVaoDangChinhThuc;
    }

    public String getNgayVaoDoan() {
        return ngayVaoDoan;
    }

    public void setNgayVaoDoan(String ngayVaoDoan) {
        this.ngayVaoDoan = ngayVaoDoan;
    }

    public String getDepartmentName() {
        return departmentName;
    }

    public void setDepartmentName(String departmentName) {
        this.departmentName = departmentName;
    }

    public String getMajorName() {
        return majorName;
    }

    public void setMajorName(String majorName) {
        this.majorName = majorName;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public Integer getLevelMax() {
        return levelMax;
    }

    public void setLevelMax(Integer levelMax) {
        this.levelMax = levelMax;
    }

    public Integer getLevelMin() {
        return levelMin;
    }

    public void setLevelMin(Integer levelMin) {
        this.levelMin = levelMin;
    }

    public Integer getIat() {
        return iat;
    }

    public void setIat(Integer iat) {
        this.iat = iat;
    }

    public Integer getExp() {
        return exp;
    }

    public void setExp(Integer exp) {
        this.exp = exp;
    }

}
