﻿using System;
using System.Collections.Generic;

namespace CoffeeShopProject.Models
{
    public partial class LoaiThucDon
    {

        public int MaLoai { get; set; }
        public string TenLoai { get; set; }
        public int? MaLoaiCha { get; set; }
    }
}
