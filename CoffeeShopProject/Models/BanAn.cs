﻿using System;
using System.Collections.Generic;

namespace CoffeeShopProject.Models
{
    public partial class BanAn
    { 
        public int MaBan { get; set; }
        public string TenBan { get; set; }
        public int MaTang { get; set; }
    }
}
