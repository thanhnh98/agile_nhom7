﻿using System;
using System.Collections.Generic;

namespace CoffeeShopProject.Models
{
    public partial class NgThamGia
    {
        public int MaNgThamGia { get; set; }
        public int MaTaiKhoan { get; set; }
        public int MaPhongChat { get; set; }
    }
}
