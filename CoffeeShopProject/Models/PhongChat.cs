﻿using System;
using System.Collections.Generic;

namespace CoffeeShopProject.Models
{
    public partial class PhongChat
    {
        public int MaPhongChat { get; set; }
        public string TenPhongChat { get; set; }
    }
}
